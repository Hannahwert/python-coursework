import random
#TODO********************************************************************************************************
#Step 4 :D
#TODO *******************************************************************************************************
class Request:
    def __init__(self,ID):
        self.ID = ID

    def fill(self,password,firstname,surname,year,campus,programme,module,days,time,campusint,programmeint,moduleint,daysint,timeint):
        self.password = password
        self.firstname = firstname
        self.surname = surname
        self.programme = programme
        self.year = year
        self.campus = campus
        self.module = module
        self.days = days
        self.time = time
        self.daysint = daysint
        self.timeint = timeint
        self.programmeint = programmeint
        self.moduleint = moduleint
        self.campusint = campusint

    def displayAll(self):#Prints all
        print("ID = ",self.ID)
        print("Password =",self.password)
        print("First Name = ",self.firstname)
        print("Surname = ",self.surname)
        print("Programme = ",self.programme)
        print("Year of Study = ",self.year)
        print("Campus of Study = ",self.campus)
        print("Module title = ",self.module)
        print("Days Available = ",self.days)
        print("Times Available on those days = ",self.time)

    def displayRestricted(self):#Prints Restricted
        print("Programme = ",self.programme)
        print("Year of Study = ",self.year)
        print("Campus of Study = ",self.campus)
        print("Module title = ",self.module)
        print("Days Available = ",self.days)
        print("Times Available on those days = ",self.time)

IDlist = []
ModuleDict = {1:["Maths for Computing","Programming","Computing Systems"],2:["Business And Enterprise","Business","Captial Management"],3:["Networking","Computer Networking","Virtual Networking"]}
programmes = ["Computer Science","Business Tech","Computer Networking"]
DaysDict = {1:"Monday",2:"Tuesday",3:"Wednesday",4:"Thursday",5:"Fridat",6:"Saturday",7:"Sunday"}
TimeDict ={1:"Morning",2:"Afternoon",3:"Evening"}
CampusList = {1:"Paisley",2:"Hamilton",3:"London",4:"Ayr"}
#Lists and Dicts required in programme

while True:
    x = random.randint(100000,999999)#Generate rand 6 digit number and check that it is unique

    if x in IDlist:
        print("Fail",x)
    else:
        IDlist.append(x)
        print("Pass", x)
        break

myRequest = Request(x)

while True:
    while True:#Start of Request
        print("Welcome to Your UWS Study Buddie!")

        print("1 - Create New Request")
        print("2 - Quit")
        choice = input(">?")

        if choice == "2":
            break

        firstname = input("First name")
        surname = input("Surname")
        password = input("Enter Password")

        while True:#Campus
            check = True
            year = input("What year of your Study are you in? Please enter in a number form.")

            try:# Checks to see if year is a integer.
                year = int(year)
            except:
                print("Please enter a number for your year of study! I.E 4 for Fourth Year")
                check = False

            if check == True:
                break

        while True:#Campus
            check = True
            print("Select your Campus!")
            print("1 - Paisley")
            print("2 - Hamilton")
            print("3 - London")
            print("4 - Ayr")
            campusint = input(">?")

            try:#Checks that it is a number
                campusint = int(campusint)
            except:
                check = False

            if check == True:
                campusint = int(campusint)
                if campusint < 1 or campusint > 4: #validate selection
                    print("Please select a valid campus!")
                else:
                    campus = CampusList[campusint]#Create Campus "String" varible
                    break

        while True:#Programme
            check = True
            print("Select your Programme of Study!")
            print("1 - Computer Science")
            print("2 - Business Tech")
            print("3 - Computer Networking")

            programmeint = input("?>")

            try:#Checks that a interger was entered
                programmeint = int(programmeint)
            except:
                print("Please enter a number from 1 to 3!")
                check = False

            if check == True:

                programmeint = int(programmeint)

                if programmeint < 1 or programmeint > 3: #validate programme selection
                    print("Please select a valid course!")
                else:
                    programme = programmes[programmeint-1]#Aquire Titles of Programme
                    break

        while True:#Module
            i = 1
            print("What Module are you looking for a \"Studdy Buddie\" in?")
            check = True

            for eachmodule in ModuleDict[programmeint]:#Finds modules for selected programme and prints them
                print(i,"-",eachmodule)
                i = i + 1

            moduleint = input(">?")#aquired module choice as int

            try:#Checks that it is a number
                moduleint = int(moduleint)
            except:
                print("Please enter a number!")
                check = False

            if check == True:
                if moduleint < 1 or moduleint > 3: #validate selection
                    print("Please select a valid module!")
                else:
                    temp = []
                    moduleint = moduleint - 1 #Decrement to call correct list index
                    temp = ModuleDict[programmeint]#Fills temp list with moduesl from selected course
                    module = temp[moduleint]#Aquire module list for selected course then set the "module" varible to the module title
                    break

        while True:#Days
            daysint= []
            days = []
            checkint = True
            check = True
            print("What Days of the Week are you free? (You can select multiple days - No Spaces between numbers)")
            print("1 - Monday")
            print("2 - Tuesday")
            print("3 - Wednesday")
            print("4 - Thursday")
            print("5 - Friday")
            print("6 - Saturday")
            print("7 - Sunday")
            choice = (input("?>"))#Choice of Day

            for eachnumber in choice:#Break up choice numbers into individual items

                try:#Checks that input is a integer
                    eachnumber = int(eachnumber)
                except:
                    print("Please enter a number!")
                    checkint = False
                    break

                if int(eachnumber) < 1 or int(eachnumber) >7:#Checks that valid integer was inputed
                    print("Please enter a valid choice!")
                    check = False
                    break
                else:
                    check = True

            if check == True and checkint == True:
                for eachnumber in choice:
                    days.append(DaysDict[int(eachnumber)])#Create list of "string" days using a dict of days
                    daysint.append(int(eachnumber))#Create list of "Int" days
                    print(daysint)
                break

        while True:#Time
            x = len(days)
            time = {}
            timeint = {}
            i = 0
            check = False

            while i < x:
                tempday = []#Reset temp lists for each day
                tempint = []
                check = True
                checkint = True
                print("What time(s) of the day are you free on - ",days[i],"No Spaces Between numbers")
                print("1 - Morning")
                print("2 - Afternoon")
                print("3 - Evening")
                choice = (input("?>"))

                for eachnumber in choice:#Break up choice numbers into individual items

                    try: #Checks that input is a number
                        eachnumber = int(eachnumber)
                    except:
                        print("Please enter a number!")
                        checkint = False
                        i = i - 1#Decrement i so that check is still on correct day
                        break

                    if int(eachnumber) < 1 or int(eachnumber) >3:#Checks that input is a valid number
                        print("Please enter a valid choice!")
                        check = False
                        i = i - 1#Dectement i so that check is still on correct day
                        break

                    else:
                        check = True

                    if check == True and checkint == True:
                            tempint.append(int(eachnumber))#Creates a list of "int" times
                            tempday.append(TimeDict[int(eachnumber)])#Creates  list of "String" times
                    else:
                        break

                i = i + 1#Increase loop

            for i in range(x):
                timeint[daysint[i]] = tempint#Create list of "int" times
                time[days[i]] = tempday #Create a list of "String" times
                print(timeint)
                print(time)

            if check == True and checkint == True:
                break
            else:
                print("Error - Restarting Selection")

        myRequest.fill(password,firstname,surname,year,campus,programme,module,days,time,campusint,programmeint,moduleint,daysint,timeint)
        myRequest.displayRestricted()
        z = input("Is this info Correct? Y/N")
        if z =="Y" or z =="y":
            break #Break overarching loop
        else:
            print("Starting again! Here we go!")
    break




myRequest.displayAll()

