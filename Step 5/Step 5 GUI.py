import easygui
import random
import pickle
import os.path
import os
#TODO********************************************************************************************************

#TODO *******************************************************************************************************
class Request:

    # constructor
    def __init__(self,ID):
        self.ID = ID

    # fills the object with data from parameters
    def fill(self, password, firstname, surname, year, campus, programme, module, days, time):
        self.password = password
        self.firstname = firstname
        self.surname = surname
        self.programme = programme
        self.year = year
        self.campus = campus
        self.module = module
        self.days = days
        self.time = time


    def displayAll(self):#Prints all information on a msgbox

        text = "ID = " +  self.ID + ",\n" + "Password =" +  self.password + ",\n" + "First Name = " +  self.firstname + ",\n" + "Surname = " +  self.surname + ",\n" + "Programme = " +  self.programme + ",\n" + "Year of Study = " +  str(self.year) + ",\n" + "Campus of Study = " +  self.campus + ",\n" + "Module title = " +  self.module + ",\n" + "Days Available = " +  str(self.days) + ",\n" + "Times Available on those days = " +  str(self.time)
        easygui.msgbox(  text, "Display All" )


    def displayRestricted(self):#Prints Restricted in a msgbox
        text = "Programme = " +  self.programme + ",\n" + "Year of Study = " +  str(self.year) + ",\n" + "Campus of Study = " +  self.campus + ",\n" + "Module title = " +  self.module + ",\n" + "Days Available = " +  str(self.days) + ",\n" + "Times Available on those days = " +  str(self.time)
        easygui.msgbox(  text, "Display Restricted" )

#Delete File
def deletefile(filename,extension):

    #Converts ID to string
    filename = str(filename)

    #Converts ID to a String version for file opening
    filename = (filename + extension)

    #Checks if file exists
    z = os.path.isfile(filename)

    if z:
        os.remove(filename)

#Creates File
def makeFile(request):# Takes in Object request

    x = request.ID# Sets x to ID of created Request
    x = str(x)#Converts ID to a String version for file opening
    x = (x + ".pck")#Concatinates the request ID to file extension

    f = open(x, "wb")#Open file

    pickle.dump(request,f)#Place object in file
    f.close()
    print( "made the file!")


#Reads FIle
def readFile(ID):#Takes in ID of request

    #print("READING FILE!")

    x = str(ID)#Converts ID to string
    x = (x + ".pck")#Converts ID to a String version for file opening
    f = open(x, "rb")#Concatinates the request ID to file extension

    loadedRequest = pickle.load(f)#Loads object within file to new varible
    f.close()
    print( "read the file!")
    return loadedRequest


#Adds ID to List of IDs in file
def addID(ID):#Takes in ID to be added and current list of IDs
    #print("ADD ID")
    while True:
        z = checkfile("Requests", ".txt")

        if z:

            IDList = getIDLIST()

            IDList.append(ID)#Adds new ID to list of IDs

            deletefile("Requests", ".txt")

            f = open("Requests.txt", "wb")
            pickle.dump(IDList,f)#Puts new IDlist into file
            f.close()

            break


#Acquires ID list from file
def getIDLIST():
    z = checkfile("Requests", ".txt")

    if z:
        f = open("Requests.txt", "rb")
        RequestList = pickle.load(f)
        f.close()#Loads in all the file names for the stored objects into alist
        return RequestList


#Checks if passed ID is in file
def checkID(ID):#Takes in ID to be checked
    while True:
        z = checkfile("Requests", ".txt")

        if z:

            IDList = getIDLIST()

            if ID in IDList:



                return False

            else:
                return True

        else:
            f = open("Requests.txt", "wb")
            pickle.dump(EmptyList, f)
            f.close()


#Checks if file exists
def checkfile(filename,extension):
    while True:
        filename = str(filename)#Converts filename#  to string
        filename = (filename + extension)#Concatinates with extension for file opening

        z = os.path.isfile(filename)#Checks if file exsists
        if z:
            return True

        elif filename == "Request":#Creats Request file if it does not exist
            f = open(filename, "wb")
            pickle.dump(EmptyList, f)
            f.close()
        else:
            easygui.msgbox("File does not Exist!" )#Prints error message that file does not exist
            return False


#Creates a new request
def createrequest():

    while True:

        ID = generateID()#generate ID

        firstname, surname, password = names()#Take in Names And Password
        year = inputyear()#Take in year of study
        campus = inputcampus()#Take in campus
        programme = inputprogramme()#take in programme
        module = inputmodule(programme)#take in module
        days = inputdays()#take in days of week free
        time = inputtimes(days)#take in times free

        myRequest = Request(ID)#Create Class

        myRequest.fill(password, firstname, surname, year, campus, programme, module, days, time)
        #Fill Request

        while True:
            myRequest.displayRestricted()#Display Restricted Info

            confirm = easygui.ynbox("Is this info Correct?","Correct?")#ASks user if info is correct

            if confirm:
                addID(myRequest.ID)
                makeFile(myRequest)#Store current request in a file
                sorting(myRequest)#Find matches to this file
                break

            else:

                # open the gui and present options. Choices are zero indexed
                choice = easygui.indexbox("What would you like to do?","Start Again Or Edit",("Edit a Part of the Request","Start Again"))


                if choice == 0:
                    addID(myRequest.ID)
                    makeFile(myRequest)#Store current request in a file
                    myRequest = editFile(myRequest.ID)#Opens edit file function to check what part of file to be edited

                elif choice == 1:
                    easygui.msgbox("Starting again! Here we go!")
                    break

        break
#DONE


#Edits a File(d) request
def editFile(ID):
    myRequest = readFile(ID)

    while True:
        password = "Password - '" + myRequest.password + "'"
        firstname = ", FirstName - '" + myRequest.firstname + "'"
        surname = ", Surname - '" + myRequest.surname + "'"
        nameschoice = "0 - " + password + firstname + surname

        year = "1 - Year - " + str(myRequest.year)
        campus = "2 - Campus - " + myRequest.campus
        programme ="3 - Programme - " + myRequest.programme
        module = "4 - Module - " + myRequest.module
        days = "5 - Days - " + ' '.join(myRequest.days)
        time = "6 - Time - " + str(myRequest.time)

        choices = [nameschoice,year,campus,programme,module,days,time]#Creates a list of choices for the Choice Box

        while True :
            choice = easygui.choicebox("What Part Would you like to edit?","Edit Request",choices=choices)

            if choice == None :
                easygui.msgbox( "Please make a choice")
            else:
                break

        # get the number from the start of each option in the choicebox
        selection = choice.split()[0]

        print(selection)
        if selection == "0":
            firstname, surname, password = names()
            myRequest.firstname = firstname
            myRequest.surname = surname
            myRequest.password = password
            #Aquire first name and last and password and change it in request
        elif selection == "1":
            year = inputyear()
            myRequest.year = year
            #Aquire year and change year in request
        elif selection == "2":
            campus =  inputcampus()
            myRequest.campus = campus
            #Aquire campus and change it in request
        elif selection == "3":
            programme = inputprogramme()
            myRequest.programme = programme
            #Aquire programme and change it in request
            easygui.msgbox("Since you have selected a new programme you will also have to pick a new module!")

            module = inputmodule(myRequest.programme)

            myRequest.module = module
            #Aquire module and change it in request
        elif selection == "4":
            module = inputmodule(myRequest.programme)

            myRequest.module = module
            #Aquire module and change it in request

        elif selection == "5":
            days = inputdays()
            time = inputtimes(days)
            myRequest.days = days
            myRequest.time = time
            #Aquire days and time and change them in request

        elif selection == "6":
            time = inputtimes(myRequest.days)
            myRequest.time = time
            #Aquire time and change them in request

        z = easygui.ynbox("Is that all the Information you would like to change?","Info Check")

        if z:#checks that information entered is what the user wants
            break

    return myRequest
#DONE

#Generated a random ID
def generateID():
    while True:

        x = random.randint(100000, 999999)#Generate rand 6 digit number and check that it is unique
        IDcheck = checkID(x)

        if IDcheck == True:
            print("Passed", x)
            return x
        else:
            print("Failed", x)


#Takes in names
def names():
    fields = ["FirstName","Surname","Password"]
    values = []
    message = "Please Enter your Name and Password"
    title = "Names & Password"


    while True : # input all values in a multi-line enter box
        values = easygui.multenterbox(message,title,fields)

        # if no values have been input
        if values == None :
            easygui.msgbox( "Please enter values")
        else :
            break #break out of the while statement

    while True : # check if any of the fields has been mised
        errmsg = ""
        for i in range(len(fields)):
          if values[i].strip() == "":
            errmsg = errmsg + fields[i] + " is a required field.\n"
        if errmsg == "":
            break # no problems found
        values = easygui.multenterbox(errmsg, title, fields, values) # ask the input again

    print( values )
    return values #firstname, surname, password
#DONE

#Takes in Year
def inputyear():
        while True: # make sure a year is input
            year = easygui.choicebox("What year of Study are you in?","Year",[1,2,3,4,5])
            if year == None : # if no year has been input
                easygui.msgbox("Please select a year")
            else:
                break

        return year
#DONE

#Takes in Campus
def inputcampus ():

        while True : # loop until a campus is selected
            campus = easygui.choicebox("Select Your Campus!","Campus",CampusList)
            if campus == None :
                easygui.msgbox( "Please select your campus")
            else :
                break

        return campus
#DONE

#Takes in programme
def inputprogramme():

    while True:#Programme

        programme = easygui.choicebox("Select your Programme of Study!","Select your Programme of Study", programmes  )

        if programme == None :
            easygui.msgbox( "Please select your program of study")
        else:
            break

    return programme


#Takes in module
def inputmodule(programme):
     while True:#Module

            choice = easygui.choicebox("What Module are you looking for a \"Studdy Buddie\" in?", "Choose Buddie", ModuleDict[ programmes.index( programme ) ])
            if choice == None :
                easygui.msgbox("Please choose a module")
            else :
                return choice


#Takes in Days free
def inputdays():
    while True:#Days


            dayschoices = [ str(k)+" "+v for k,v in DaysDict.items() ] # create a list of choices of days from the days dictionary

            while True :
                dayschosen = easygui.multchoicebox("What Days of the Week are you free?","Choose Days", dayschoices)

                if dayschosen == None:
                    easygui.msgbox( "Please choose the days of the week")
                else:
                    break

            print( dayschosen)

            days = [] # the list of strings of days taken from the days that are available
            for daychosen in dayschosen :
                days.append( daychosen.split()[1] )

            print (days)

            return days


#Takes in times free on those days
def inputtimes(days):
     while True:#Time

            timechoices = [ str(k)+" "+v for k,v in TimeDict.items() ] # create a list of the times available
            print( timechoices )

            time = {} # the dictionary that will hold the days and times that are chosen
            for day in days : #for each day that has been chosen

                while True : # get times
                    timechosen = easygui.multchoicebox("What times on " + day + " are you free?","Choose Times", timechoices)
                    if timechosen == None :
                        easygui.msgbox( "Please choose times for " + day)
                    else:
                        break

                print( timechosen )

                timeschosen = [] #this is the list of the chosen times
                for timestring in timechosen :
                    timeschosen.append( timestring.split()[1] ) # add each time to this list

                print( timeschosen )

                time[day] = timeschosen # assign the times chosen to the day


            return time


#Display matches to current option
def displayMatches(dictOfMatches, countOfMatches):
    #print("DISPLAYING!")
    i = 5

    if countOfMatches == 0 or len(dictOfMatches) == 0:

        easygui.msgbox("No Buddies found!")
    else:
        easygui.msgbox("Matching study buddies : " + str(countOfMatches) )
        while i > 0:

            listOfMatches = dictOfMatches[i]

            if len(listOfMatches)> 0:
                easygui.msgbox("These have " + str(i) + " matching attributes to you!")
                for eachid in listOfMatches:
                    matchedrequest = readFile(eachid)
                    matchedrequest.displayRestricted()
            i -= 1


#Sorts Matches
def sorting(myRequest):
    #Lists that store matched objects
    matchedList5 = []
    matchedList4 = []
    matchedList3 = []
    matchedList2 = []
    matchedList1 = []

    RequestList = getIDLIST()#Gets list of IDs from Requests.txt

#    newLine()
    easygui.msgbox("We will now look for your Study Buddie!")
#    newLine()

    matchesCount = 0  #Count the number of matches
    a = 0 #Varible to determine how many matching attributes a pair has
    daysmatch = 0#Count number of days that match in each pair
    truedaycheck = 0#Holds a more "True" value of matches
    timematch = 0#Counts number of times that match in each pair
    timecount = 0#Counts how many times are in the loaded request
    truetimematch = 0#Holds a more "True" value of matches

    for eachrequest in RequestList:#works through each request

        z = checkfile(eachrequest, ".pck")#checks if file exists

        if z:
            LoadedObject = readFile(eachrequest)#Loads in each object from file names

            if LoadedObject.ID != myRequest.ID:#Makes sure that the Loaded object is not the current object.
                a = 0

                if LoadedObject.module == myRequest.module:#Checks to see if the two objects have the same module
                    matchesCount += 1# Increase matches count
                    a = 1

                    for eachdayload in LoadedObject.days:#For each day in the loaded objects list of days
                        if eachdayload in myRequest.days:#Checks if each day from loaded object is in current object
                            daysmatch += 1#Increments the counter if the loaded object days match the current object

                            for eachtimeload in LoadedObject.time[eachdayload]:#For each time within each day
                                timecount += 1
                                if eachtimeload in myRequest.time[eachdayload]:
                                    timematch +=1

                    truedaymatch = daysmatch/len(LoadedObject.days)#Divides the number of matching days by how many days have been chosen to give a measure of matching days that is inline with other days
                    truetimematch = timematch/timecount#Divides number of matching times by total number of times to give a measure that is more consistent
                    #Doing this allows for requests to match with more similar requests EG - If the current request is only frree on monday morning - Request 1 Is free Monday Morning and Evening Request two is Free Monday Morning only - This small algorithm will display the Monday Morning only first

                    if truedaymatch > 0.5:
                        a += 1
                    if truetimematch > 0.5:
                        a += 1
                #    if LoadedObject.time == myRequest.time:
                #        a += 1
                    if LoadedObject.campus == myRequest.campus:
                        a += 1
                    if LoadedObject.year == myRequest.year:
                        a += 1
                    #Checks all attributes and increase the number of attributes that match with a+= 1

                    if a == 1:
                        matchedList1.append(LoadedObject.ID)
                    elif a == 2:
                        matchedList2.append(LoadedObject.ID)
                    elif a == 3:
                        matchedList3.append(LoadedObject.ID)
                    elif a == 4:
                        matchedList4.append(LoadedObject.ID)
                    elif a == 5:
                        matchedList5.append(LoadedObject.ID)
                    #Does a super lame sort to put objects of higher number of matches in to "higher" lists e.g 4 Matching attributes goes into matched list 4

    matchedDict = {1: matchedList1, 2: matchedList2, 3: matchedList3, 4: matchedList4, 5: matchedList5}
    #Makes a dictionary of all the lists which contain the matches

    displayMatches(matchedDict, matchesCount)#This function displays the matches#Sorts Matches


#Checks if password is valid
def checkpassword(password, ID):

    check = checkfile(ID, ".pck")#Checks if file exsists

    if check:
        request = readFile(ID)#Gets requests object from file using ID

    if request.password == password:#Returns boolen if password is correct
        return True
    else:
        return False

#create random students
def createstudents():

    # get the random names
    names = []
    with open("names.txt") as file:
        for name in file:
            names.append(name)

    for name in names :
        print( name )
        ID = generateID()#generate ID
        myRequest = Request(ID)#Create Class
        addID(myRequest.ID)

        myRequest.firstname = name.split()[0]
        myRequest.surname = name.split()[1]
        myRequest.password = "hello"
        myRequest.year = random.randint(1,5)

        myRequest.campus = CampusList[ random.randint(0,3) ]
        programint = random.randint( 0, len(programmes)-1)
        myRequest.programme = programmes[ programint ]
        myRequest.module = ModuleDict[programint][ random.randint(0,2)]

        dayschoices = [ v for k,v in DaysDict.items() ]
        while True :
            days = []
            for day in dayschoices :
                if random.randint(1,100) < 40 :
                    days.append(day)
            if len(days) > 0 :
                break

        myRequest.days = days

        time = {}
        timechoices = [ v for k,v in TimeDict.items() ]
        for day in myRequest.days :
            while True :
                times = []
                for timechoice in timechoices :
                    if random.randint(1,100) < 30 :
                        times.append( timechoice )
                if len( times ) > 0 :
                    break
            time[ day ] = times

        myRequest.time = time

        makeFile(myRequest)#Store current request in a file


EmptyList = [] #Just an empty list for filling a file


# string constants for the program
ModuleDict = {0: ["Maths for Computing", "Programming", "Computing Systems"], 1: ["Business And Enterprise", "Business", "Capital Management"], 2: ["Networking", "Computer Networking", "Virtual Networking"]}
programmes = ["Computer Science", "Business Tech", "Computer Networking"]
DaysDict = {1: "Monday", 2: "Tuesday", 3: "Wednesday", 4: "Thursday", 5: "Friday", 6: "Saturday", 7: "Sunday"}
TimeDict ={1: "Morning", 2: "Afternoon", 3: "Evening"}
CampusList = ["Paisley","Hamilton","London","Ayr"]
#Lists of values used for menus in input section


while True:#Start of Request

    choices = ["1 - Create New Request", "2 - Edit Existing Request", "3 - Quit", "0 - Create Random Students"]

    while True : # make sure an option is chosen
        choice = easygui.choicebox( "Select an Option", "Welcome to Your UWS Study Buddie!", choices)
        if choice == None :
            easygui.msgbox( "Please select an option")
        else:
            break

    selection = choice.split()[0] # get the number of the chosen optoin
    print( selection )

    if selection == "1": # create a new request
        createrequest()#Function to create a request

    elif selection == "2": # edit an existing request
        while True:

            while True : # get the id number
                ID = easygui.enterbox( "Enter your ID Number", "Enter ID")

                if ID == None :
                    easygui.msgbox( "Please enter your ID")
                else :
                    break
#            ID = input("Enter your ID Please")

            z = checkID(ID)#Check ID checks if the ID is in the ID list
            y = checkfile(ID, ".pck")#Checks if the file exists

            if z and y:

                while True : # get the password
                    password = easygui.enterbox( "Enter Your Password", "Enter Password" )
                    if password == None :
                        easygui.msgbox( "Please Enter Your Password")
                    else :
                        break

                check = checkpassword(password, ID)#Check password checks if password is correct

                if check:
                    editedRequest = editFile(ID)#Creates new request as edited request
                    deletefile(editedRequest.ID, ".pck")#Deletes old request file
                    makeFile(editedRequest)#creates a new file with the new edited request
                    sorting(editedRequest)#Call matching algorithm
                    easygui.msgbox("Search Complete!")
                    break
                else :
                    easygui.msgbox( "Incorrect Password. Try again")

    elif selection == "3":
        easygui.msgbox("Thank you!")
        break
    elif selection == "0" :
        createstudents()
