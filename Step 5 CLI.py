import random
import pickle
import os.path
import os
#TODO********************************************************************************************************

#TODO *******************************************************************************************************
class Request:
    def __init__(self,ID):
        self.ID = ID

    def fill(self, password, firstname, surname, year, campus, programme, module, days, time, campusint, programmeint, moduleint, daysint,timeint):
        self.password = password
        self.firstname = firstname
        self.surname = surname
        self.programme = programme
        self.year = year
        self.campus = campus
        self.module = module
        self.days = days
        self.time = time
        self.daysint = daysint
        self.timeint = timeint
        self.programmeint = programmeint
        self.moduleint = moduleint
        self.campusint = campusint

    def displayAll(self):#Prints all
        print("ID = ", self.ID)
        print("Password =", self.password)
        print("First Name = ", self.firstname)
        print("Surname = ", self.surname)
        print("Programme = ", self.programme)
        print("Year of Study = ", self.year)
        print("Campus of Study = ", self.campus)
        print("Module title = ", self.module)
        print("Days Available = ", self.days)
        print("Times Available on those days = ", self.time)

    def displayRestricted(self):#Prints Restricted
        print("Programme = ", self.programme)
        print("Year of Study = ", self.year)
        print("Campus of Study = ", self.campus)
        print("Module title = ", self.module)
        print("Days Available = ", self.days)
        print("Times Available on those days = ", self.time)
        newLine()


def deletefile(filename,extension):

    filename = str(filename)#Converts ID to string
    filename = (filename + extension)#Converts ID to a String version for file opening

    z = os.path.isfile(filename)#Checks if file exsists

    if z:
        os.remove(filename)
#Delete File


def makeFile(request):# Takes in Object request
    #print("MAKE FILE!")

    x = request.ID# Sets x to ID of created Request
    x = str(x)#Converts ID to a String version for file opening
    x = (x + ".pck")#Concatinates the request ID to file extension

    f = open(x, "wb")#Open file

    pickle.dump(request,f)#Place object in file
    f.close()
#Creates File


def readFile(ID):#Takes in ID of request

    #print("READING FILE!")

    x = str(ID)#Converts ID to string
    x = (x + ".pck")#Converts ID to a String version for file opening
    f = open(x, "rb")#Concatinates the request ID to file extension

    loadedRequest = pickle.load(f)#Loads object within file to new varible
    f.close()
    return loadedRequest
#Reads FIle


def addID(ID):#Takes in ID to be added and current list of IDs
    #print("ADD ID")
    while True:
        z = checkfile("Requests", ".txt")

        if z:

            IDList = getIDLIST()

            IDList.append(ID)#Adds new ID to list of IDs

            deletefile("Requests", ".txt")

            f = open("Requests.txt", "wb")
            pickle.dump(IDList,f)#Puts new IDlist into file
            f.close()

            break
#Adds ID to List of IDs in file


def getIDLIST():
    z = checkfile("Requests", ".txt")

    if z:
        f = open("Requests.txt", "rb")
        RequestList = pickle.load(f)
        f.close()#Loads in all the file names for the stored objects into alist
        return RequestList
#Aquires ID list from file


def checkID(ID):#Takes in ID to be checked
    while True:
        z = checkfile("Requests", ".txt")

        if z:

            IDList = getIDLIST()

            if ID in IDList:

                print("Current Length of ID List", len(IDList))

                return False

            else:
                return True

        else:
            f = open("Requests.txt", "wb")
            pickle.dump(EmptyList, f)
            f.close()
#Checks if passed ID is in file


def newLine():
    print()
#Creates a new line in CLI


def checkfile(filename,extension):
    while True:
        filename = str(filename)#Converts filename#  to string
        filename = (filename + extension)#Concatinates with extension for file opening

        z = os.path.isfile(filename)#Checks if file exsists
        if z:
            return True

        elif filename == "Request":#Creats Request file if it does not exist
            f = open(filename, "wb")
            pickle.dump(EmptyList, f)
            f.close()
        else:
            print("File does not Exist!")#Prints error message that file does not exist
            return False
#Checks if file exists


def createrequest():

    while True:

        ID = generateID()#generate ID

        firstname, surname, password = names()#Take in Names And Password
        year = inputyear()#Take in year of study
        campusint, campus = inputcampus()#Take in campus
        programmeint, programme = inputprogramme()#take in programme
        moduleint, module = inputmodule(programmeint)#take in module
        daysint, days = inputdays()#take in days of week free
        timeint, time = inputtimes(days, daysint)#take in times free

        myRequest = Request(ID)#Create Class

        myRequest.fill(password, firstname, surname, year, campus, programme, module, days, time, campusint, programmeint, moduleint, daysint, timeint)
        #Fill Request

        while True:
            myRequest.displayRestricted()#Display Restricted Info

            confirm = input("Is this info Correct? Y/N")#ASks user if info is correct

            if confirm =="Y" or confirm =="y" or confirm == "1":
                addID(myRequest.ID)
                makeFile(myRequest)#Store current request in a file
                sorting(myRequest)#Find matches to this file
                break

            else:
                print("Would you like to --")
                print("1 - Edit a part of the Request")
                print("2 - Start Again")
                choice = int(input("?> "))

                if choice == 1:
                    myRequest = editCurrentRequest(myRequest)#Opens edit file function to check what part of file to be edited

                elif choice == 2:
                    print("Starting again! Here we go!")
                    break

        break
#Creates a new request


def editFile(ID):
    myRequest = readFile(ID)

    while True:
        print("What part would you like to edit?")
        print("1 - Your First name Surname and Password")
        print(myRequest.firstname, myRequest.surname, myRequest.password)
        print("2 - Your Year of Study")
        print(myRequest.year)
        print("3 - Your Campus")
        print(myRequest.campus)
        print("4 - Your Programme")
        print(myRequest.programme)
        print("5 - Your Module")
        print(myRequest.module)
        print("6 - The Days & Times that you are free")
        print(myRequest.days)
        print(myRequest.time)
        choice = int(input("?> "))

        if choice == 1:
            firstname, surname, password = names()
            myRequest.firstname = firstname
            myRequest.surname = surname
            myRequest.password = password
            #Aquire first name and last and password and change it in request
        elif choice == 2:
            year = inputyear()
            myRequest.year = year
            #Aquire year and change year in request
        elif choice == 3:
            campusint, campus =  inputcampus()
            myRequest.campusint = campusint
            myRequest.campus = campus
            #Aquire campus and change it in request
        elif choice == 4:
            programmeint, programme = inputprogramme()
            myRequest.programmeint = programmeint
            myRequest.programme = programme
            #Aquire programme and change it in request
            print("Since you have selected a new programme you will also have to pick a new module!")

            moduleint, module = inputmodule(myRequest.programmeint)
            myRequest.moduleint = moduleint
            myRequest.module = module
            #Aquire module and change it in request
        elif choice == 5:
            moduleint, module = inputmodule(myRequest.programmeint)
            myRequest.moduleint = moduleint
            myRequest.module = module
            #Aquire module and change it in request

        elif choice == 6:
            daysint, days = inputdays()
            timeint, time = inputtimes(daysint,days)
            myRequest.daysint = daysint
            myRequest.days = days
            myRequest.timeint = timeint
            myRequest.time = time
            #Aquire days and time and change them in request

        z = input("Is that the only information you want to change? Y/N ")

        if z == "Y" or z == "y":#checks that information entered is what the user wants
            break

    return myRequest
#Edits a File


def generateID():
    while True:

        x = random.randint(100000, 999999)#Generate rand 6 digit number and check that it is unique
        IDcheck = checkID(x)

        if IDcheck == True:
            print("Passed", x)
            return x
        else:
            print("Failed", x)
#Generated a random ID


def names():

    firstname = input("First name? ")
    surname = input("Surname? ")
    password = input("Please enter a Password ")

    return firstname, surname, password
#Takes in names


def inputyear():
     while True:#Campus
            check = True
            year = input("What year of your Study are you in? Please enter in a number form. ")

            try:# Checks to see if year is a integer.
                year = int(year)
            except:
                print("Please enter a number for your year of study! I.E 4 for Fourth Year")
                check = False

            if check:
                return year
#Takes in Year


def inputcampus ():
     while True:#Campus
            check = True
            print("Select your Campus!")
            print("1 - Paisley")
            print("2 - Hamilton")
            print("3 - London")
            print("4 - Ayr")
            campusint = input("?> ")

            try:#Checks that it is a number
                campusint = int(campusint)
            except:
                check = False

            if check:
                campusint = int(campusint)
                if campusint < 1 or campusint > 4: #validate selection
                    print("Please select a valid campus!")
                else:
                    campus = CampusList[campusint]#Create Campus "String" varible
                    return campusint, campus
#Takes in Campus


def inputprogramme():
     while True:#Programme
            check = True
            print("Select your Programme of Study!")
            print("1 - Computer Science")
            print("2 - Business Tech")
            print("3 - Computer Networking")

            programmeint = input("?> ")

            try:#Checks that a interger was entered
                programmeint = int(programmeint)
            except:
                print("Please enter a number from 1 to 3!")
                check = False

            if check:

                programmeint = int(programmeint)

                if programmeint < 1 or programmeint > 3: #validate programme selection
                    print("Please select a valid course!")
                else:
                    programme = programmes[programmeint-1]#Aquire Titles of Programme
                    return programmeint, programme
#Takes in programme


def inputmodule(programmeint):
     while True:#Module
            i = 1
            print("What Module are you looking for a \"Studdy Buddie\" in?")
            check = True

            for eachmodule in ModuleDict[programmeint]:#Finds modules for selected programme and prints them
                print(i, "-", eachmodule)
                i = i + 1

            moduleint = input("?> ")#aquired module choice as int

            try:#Checks that it is a number
                moduleint = int(moduleint)
            except:
                print("Please enter a number!")
                check = False

            if check == True:
                if moduleint < 1 or moduleint > 3: #validate selection
                    print("Please select a valid module!")
                else:
                    temp = []
                    moduleint -=1 #Decrement to call correct list index
                    temp = ModuleDict[programmeint]#Fills temp list with moduesl from selected course
                    module = temp[moduleint]#Aquire module list for selected course then set the "module" varible to the module title
                    return moduleint, module
#Takes in module


def inputdays():
    while True:#Days
            daysint= []
            days = []
            checkint = True
            check = True
            print("What Days of the Week are you free? (You can select multiple days - No Spaces between numbers)")
            print("1 - Monday")
            print("2 - Tuesday")
            print("3 - Wednesday")
            print("4 - Thursday")
            print("5 - Friday")
            print("6 - Saturday")
            print("7 - Sunday")
            choice = (input("?> "))#Choice of Day

            for eachnumber in choice:#Break up choice numbers into individual items

                try:#Checks that input is a integer
                    eachnumber = int(eachnumber)
                except:
                    print("Please enter a number!")
                    checkint = False
                    break

                if int(eachnumber) < 1 or int(eachnumber) >7:#Checks that valid integer was inputed
                    print("Please enter a valid choice!")
                    check = False
                    break
                else:
                    check = True

            if check and checkint:
                for eachnumber in choice:
                    days.append(DaysDict[int(eachnumber)])#Create list of "string" days using a dict of days
                    daysint.append(int(eachnumber))#Create list of "Int" days
                    #print(daysint)
                return daysint, days
#Takes in Days free


def inputtimes(days, daysint):
     while True:#Time
            x = len(days)
            time = {}
            timeint = {}
            i = 0
            check = False

            while i < x:
                tempday = []#Reset temp lists for each day
                tempint = []
                check = True
                checkint = True
                print("What time(s) of the day are you free on - ", days[i], "No Spaces Between numbers")
                print("1 - Morning")
                print("2 - Afternoon")
                print("3 - Evening")
                choice = (input("?>"))

                for eachnumber in choice:#Break up choice numbers into individual items

                    try: #Checks that input is a number
                        eachnumber = int(eachnumber)
                    except:
                        print("Please enter a number!")
                        checkint = False
                        i = i - 1#Decrement i so that check is still on correct day
                        break

                    if int(eachnumber) < 1 or int(eachnumber) >3:#Checks that input is a valid number
                        print("Please enter a valid choice!")
                        check = False
                        i -=1 #Dectement i so that check is still on correct day
                        break

                    else:
                        check = True

                    if check and checkint:
                            tempint.append(int(eachnumber))#Creates a list of "int" times
                            tempday.append(TimeDict[int(eachnumber)])#Creates  list of "String" times
                    else:
                        break

                i +=1 #Increase loop

            for i in range(x):
                timeint[daysint[i]] = tempint#Create list of "int" times
                time[days[i]] = tempday #Create a list of "String" times


            if check == True and checkint == True:
                return timeint, time

            else:
                print("Error - Restarting Selection")
#Takes in times free on those days


def displayMatches(dictOfMatches, countOfMatches):
    #print("DISPLAYING!")
    i = 5

    if countOfMatches == 0 or len(dictOfMatches) == 0:

        print("No Buddies found!")
    else:
        print("There is", countOfMatches, " matche(s)!")
        newLine()
        while i > 0:

            listOfMatches = dictOfMatches[i]

            if len(listOfMatches)> 0:
                print("These have ", i, "matching attributes to you!")
                newLine()
            for eachid in listOfMatches:
                matchedrequest = readFile(eachid)
                matchedrequest.displayRestricted()
            i -= 1
#Display matches to current option


def sorting(myRequest):
    matchedList5 = []
    matchedList4 = []
    matchedList3 = []
    matchedList2 = []
    matchedList1 = []
    #Lists that store matched objects

    RequestList = getIDLIST()#Gets list of IDs from Requests.txt

    newLine()
    print("We will now look for your Study Buddie!")
    newLine()

    matchesCount = 0  #Count the number of matches
    a = 0 #Varible to determine how many matching attributes a pair has
    daysmatch = 0#Count number of days that match in each pair
    truedaycheck = 0#Holds a more "True" value of matches
    timematch = 0#Counts number of times that match in each pair
    timecount = 0#Counts how many times are in the loaded request
    truetimematch = 0#Holds a more "True" value of matches

    for eachrequest in RequestList:#works through each request

        z = checkfile(eachrequest, ".pck")#checks if file exists

        if z:
            LoadedObject = readFile(eachrequest)#Loads in each object from file names

            if LoadedObject.ID != myRequest.ID:#Makes sure that the Loaded object is not the current object.
                a = 0

                if LoadedObject.moduleint == myRequest.moduleint:#Checks to see if the two objects have the same module
                    matchesCount += 1# Increase matches count
                    a = 1

                    for eachdayload in LoadedObject.daysint:#For each day in the loaded objects list of days
                        if eachdayload in myRequest.daysint:#Checks if each day from loaded object is in current object
                            daysmatch += 1#Increments the counter if the loaded object days match the current object

                            for eachtimeload in LoadedObject.timeint[eachdayload]:#For each time within each day
                                timecount += 1
                                if eachtimeload in myRequest.timeint[eachdayload]:
                                    timematch +=1

                    truedaymatch = daysmatch/len(LoadedObject.daysint)#Divides the number of matching days by how many days have been chosen to give a measure of matching days that is inline with other days
                    truetimematch = timematch/timecount#Divides number of matching times by total number of times to give a measure that is more consistent
                    #Doing this allows for requests to match with more similar requests EG - If the current request is only frree on monday morning - Request 1 Is free Monday Morning and Evening Request two is Free Monday Morning only - This small algorithm will display the Monday Morning only first

                    if truedaymatch > 0.5:
                        a += 1
                    if truetimematch > 0.5:
                        a += 1
                    if LoadedObject.timeint == myRequest.time:
                        a += 1
                    if LoadedObject.campusint == myRequest.campusint:
                        a += 1
                    if LoadedObject.year == myRequest.year:
                        a += 1
                    #Checks all attributes and increase the number of attributes that match with a+= 1

                    if a == 1:
                        matchedList1.append(LoadedObject.ID)
                    elif a == 2:
                        matchedList2.append(LoadedObject.ID)
                    elif a == 3:
                        matchedList3.append(LoadedObject.ID)
                    elif a == 4:
                        matchedList4.append(LoadedObject.ID)
                    elif a == 5:
                        matchedList5.append(LoadedObject.ID)
                    #Does a super lame sort to put objects of higher number of matches in to "higher" lists e.g 4 Matching attributes goes into matched list 4

    matchedDict = {1: matchedList1, 2: matchedList2, 3: matchedList3, 4: matchedList4, 5: matchedList5}
    #Makes a dictionary of all the lists which contain the matches

    displayMatches(matchedDict, matchesCount)#This function displays the matches#Sorts Matches
#Sorts Matches


def checkpassword(password, ID):

    check = checkfile(ID, ".pck")#Checks if file exsists

    if check:
        request = readFile(ID)#Gets requests object from file using ID

    if request.password == password:#Returns boolen if password is correct
        return True
    else:
        return False
#Checks if password is valid


def editCurrentRequest(myRequest):

    while True:
        print("What part would you like to edit?")
        print("1 - Your First name Surname and Password")
        print(myRequest.firstname, myRequest.surname, myRequest.password)
        print("2 - Your Year of Study")
        print(myRequest.year)
        print("3 - Your Campus")
        print(myRequest.campus)
        print("4 - Your Programme")
        print(myRequest.programme)
        print("5 - Your Module")
        print(myRequest.module)
        print("6 - The Days & Times that you are free")
        print(myRequest.days)
        print(myRequest.time)
        choice = int(input("?> "))

        if choice == 1:
            firstname, surname, password = names()
            myRequest.firstname = firstname
            myRequest.surname = surname
            myRequest.password = password
            #Aquire first name and last and password and change it in request
        elif choice == 2:
            year = inputyear()
            myRequest.year = year
            #Aquire year and change year in request
        elif choice == 3:
            campusint, campus =  inputcampus()
            myRequest.campusint = campusint
            myRequest.campus = campus
            #Aquire campus and change it in request
        elif choice == 4:
            programmeint, programme = inputprogramme()
            myRequest.programmeint = programmeint
            myRequest.programme = programme
            #Aquire programme and change it in request
            print("Since you have selected a new programme you will also have to pick a new module!")

            moduleint, module = inputmodule(myRequest.programmeint)
            myRequest.moduleint = moduleint
            myRequest.module = module
            #Aquire module and change it in request
        elif choice == 5:
            moduleint,module = inputmodule(myRequest.programmeint)
            myRequest.moduleint = moduleint
            myRequest.module = module
            #Aquire module and change it in request

        elif choice == 6:
            daysint, days = inputdays()
            timeint, time = inputtimes(daysint, days)
            myRequest.daysint = daysint
            myRequest.days = days
            myRequest.timeint = timeint
            myRequest.time = time
            #Aquire days and time and change them in request

        z = input("Is that the only information you want to change? Y/N ")

        if z == "Y" or z == "y":#checks that information entered is what the user wants
            break
    return myRequest

EmptyList = []
#Just an empty list for filling a file

ModuleDict = {1: ["Maths for Computing", "Programming", "Computing Systems"], 2: ["Business And Enterprise", "Business", "Captial Management"], 3: ["Networking", "Computer Networking", "Virtual Networking"]}
programmes = ["Computer Science", "Business Tech", "Computer Networking"]
DaysDict = {1: "Monday", 2: "Tuesday", 3: "Wednesday", 4: "Thursday", 5: "Friday", 6: "Saturday", 7: "Sunday"}
TimeDict ={1: "Morning", 2: "Afternoon", 3: "Evening"}
CampusList = {1: "Paisley", 2: "Hamilton", 3: "London", 4: "Ayr"}
#Lists of values used for menus in input section


while True:#Start of Request
    print("Welcome to Your UWS Study Buddie!")
    newLine()

    print("Select and Option!")
    print("1 - Create New Request")
    print("2 - Edit Existing Request")
    print("3 - Quit")

    selection = int(input(">?"))

    if selection == 1:
        createrequest()#Function to create a request

    elif selection == 2:
        while True:

            ID = input("Enter your ID Please")

            z = checkID(ID)#Check ID checks if the ID is in the ID list
            y = checkfile(ID, ".pck")#Checks if the file exists

            if z and y:
                password = input("Please enter your password")

                check = checkpassword(password, ID)#Check password checks if password is correct

                if check:
                    editedRequest = editFile(ID)#Creates new request as edited request
                    deletefile(editedRequest.ID, ".pck")#Deletes old request file
                    makeFile(editedRequest)#creates a new file with the new edited request
                    sorting(editedRequest)#Call matching algorithm
                    print("Edit Complete!")
                    break

    elif selection == 3:
        print("Thank you!")
        break
